from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    details = get_object_or_404(Project, id=id)
    context = {"details": details}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        # use form to validate values and save to database
        form = ProjectForm(request.POST)
        if form.is_valid():
            # save all information from form into variable, and not the database
            project = form.save(False)
            # add logged in user as the author attribute in recipe
            project.owner = request.user
            # save author with user info
            project.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("list_projects")
    else:
        # create an instance of the Django model form class
        form = ProjectForm()
        # put the form in the context
        context = {"form": form}
        return render(request, "projects/create.html", context)
