from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        # use form to validate values and save to database
        form = TaskForm(request.POST)
        if form.is_valid():
            # save all information from form
            form.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("list_projects")
    else:
        # create an instance of the Django model form class
        form = TaskForm()
        # put the form in the context
        context = {
            "form": form,
        }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/tasklist.html", context)
